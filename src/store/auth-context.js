import { createContext } from "react";

const AuthContext = createContext({
    isLoggedin: false,
});

export default AuthContext;
