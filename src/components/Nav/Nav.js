import { useContext } from "react";
import AuthContext from "../../store/auth-context";
import styles from "./Nav.module.css";

const Nav = () => {
    const ctx = useContext(AuthContext);
    // console.log(ctx.logoutStatus);
    return (
        <nav className={styles.nav}>
            <h1> A Sample Page</h1>
            {ctx.isLoggedin && (
                <div className={styles.btns}>
                    <button className={styles.btn}>Users</button>
                    <button className={styles.btn}>Admin</button>
                    <button
                        className={`${styles.btn} ${styles.link_btn}`}
                        onClick={ctx.logoutStatus}
                    >
                        Logout
                    </button>
                </div>
            )}
        </nav>
    );
};

export default Nav;
