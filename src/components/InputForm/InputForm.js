import { useState, useContext } from "react";
import AuthContext from "../../store/auth-context";
import Card from "../UI/Card";
import styles from "./InputForm.module.css";

const InputForm = (props) => {
    const ctx = useContext(AuthContext);
    // states
    const [pass, setPass] = useState("");

    //Handlers
    const submitHandler = (event) => {
        event.preventDefault();
        if (pass.length >= 7) props.onValid(true);
    };
    const passHandler = (password) => {
        setPass(password.target.value);
    };
    // content
    let classes = "";
    let content = (
        <form
            className={`${pass.length >= 7 ? styles.active : classes}`}
            onSubmit={submitHandler}
        >
            <div>
                <label>Email</label>
                <input type="email" required />
            </div>
            <div>
                <label>Password</label>
                <input type="password" onChange={passHandler} required />
            </div>
            <button type="submit">Login</button>
        </form>
    );
    //return
    return (
        <Card className={styles.container}>
            {!ctx.isLoggedin ? content : <h2>Your welcome !</h2>}
        </Card>
    );
};

export default InputForm;
