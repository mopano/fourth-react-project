/* eslint-disable react/jsx-pascal-case */
import { useState, useEffect } from "react";
import Nav from "./components/Nav/Nav";
import InputForm from "./components/InputForm/InputForm";
import AuthContext from "./store/auth-context";

function App() {
    //states
    const [status, setStatus] = useState(false);
    useEffect(() => {
        const isLoggedin = localStorage.getItem("isLoggedin");
        if (isLoggedin === "1") setStatus(true);
    }, []);
    //Handlers
    const validHandler = () => {
        localStorage.setItem("isLoggedin", "1");
        setStatus(true);
    };
    const logoutSatusHandler = () => {
        localStorage.removeItem("isLoggedin");
        setStatus(false);
    };
    // return
    return (
        <AuthContext.Provider
            value={{
                isLoggedin: status,
                logoutStatus: logoutSatusHandler,
            }}
        >
            <Nav />
            <InputForm onValid={validHandler} />
        </AuthContext.Provider>
    );
}

export default App;
